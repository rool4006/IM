from PyQt6.QtWidgets import (
    QWidget, QLabel, QLineEdit, QPushButton, 
    QVBoxLayout, QMessageBox, QDialog, QHBoxLayout,
    QToolButton
)
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon
from database import Database

class RegisterWindow(QDialog):
    def __init__(self, db, parent=None):
        super().__init__(parent)
        self.db = db
        self.init_ui()
        
    def init_ui(self):
        self.setWindowTitle('注册')
        self.setFixedSize(350, 250)
        self.setStyleSheet('''
            QWidget {
                background-color: #f5f5f5;
                font-family: Arial;
            }
            QLabel {
                font-size: 14px;
                color: #333;
            }
            QLineEdit {
                padding: 5px;
                border: 1px solid #ccc;
                border-radius: 3px;
                font-size: 14px;
            }
            QPushButton {
                background-color: #28a745;
                color: white;
                padding: 8px;
                border: none;
                border-radius: 3px;
                font-size: 14px;
            }
            QPushButton:hover {
                background-color: #218838;
            }
        ''')
        
        # 创建控件
        self.username_label = QLabel('用户名:')
        self.username_input = QLineEdit()
        
        self.password_label = QLabel('密码:')
        # 密码输入框和可见性切换按钮
        password_layout = QHBoxLayout()
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.EchoMode.Password)
        
        self.toggle_password_button = QToolButton()
        self.toggle_password_button.setIcon(QIcon("icons/eye.png"))
        self.toggle_password_button.setCursor(Qt.CursorShape.PointingHandCursor)
        self.toggle_password_button.setStyleSheet("border: none; padding: 0;")
        self.toggle_password_button.clicked.connect(self.toggle_password_visibility)
        
        password_layout.addWidget(self.password_input)
        password_layout.addWidget(self.toggle_password_button)
        
        self.confirm_password_label = QLabel('确认密码:')
        self.confirm_password_input = QLineEdit()
        self.confirm_password_input.setEchoMode(QLineEdit.EchoMode.Password)
        
        self.register_button = QPushButton('注册')
        self.register_button.clicked.connect(self.on_register)
        
        # 布局
        layout = QVBoxLayout()
        layout.addWidget(self.username_label)
        layout.addWidget(self.username_input)
        layout.addWidget(self.password_label)
        layout.addWidget(self.password_input)
        layout.addWidget(self.confirm_password_label)
        layout.addWidget(self.confirm_password_input)
        layout.addWidget(self.register_button)
        
        self.setLayout(layout)
    
    def toggle_password_visibility(self):
        if self.password_input.echoMode() == QLineEdit.EchoMode.Password:
            self.password_input.setEchoMode(QLineEdit.EchoMode.Normal)
            self.toggle_password_button.setIcon(QIcon.fromTheme("view-visible"))
        else:
            self.password_input.setEchoMode(QLineEdit.EchoMode.Password)
            self.toggle_password_button.setIcon(QIcon.fromTheme("view-hidden"))

    def on_register(self):
        username = self.username_input.text()
        password = self.password_input.text()
        confirm_password = self.confirm_password_input.text()
        
        if not username or not password:
            QMessageBox.warning(self, '注册失败', '用户名和密码不能为空')
            return
            
        if password != confirm_password:
            QMessageBox.warning(self, '注册失败', '两次输入的密码不一致')
            return
            
        if self.db.register_user(username, password):
            QMessageBox.information(self, '注册成功', '注册成功，请登录')
            self.close()
            self.parent().show()
            self.parent().username_input.setText(username)
            self.parent().password_input.setText(password)
        else:
            QMessageBox.warning(self, '注册失败', '用户名已存在')

class LoginWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.db = Database()
        self.init_ui()
        
    def init_ui(self):
        self.setWindowTitle('登录系统')
        self.setFixedSize(350, 250)
        self.setStyleSheet('''
            QWidget {
                background-color: #f5f5f5;
                font-family: Arial;
            }
            QLabel {
                font-size: 14px;
                color: #333;
            }
            QLineEdit {
                padding: 5px;
                border: 1px solid #ccc;
                border-radius: 3px;
                font-size: 14px;
            }
            QPushButton {
                background-color: #007bff;
                color: white;
                padding: 8px;
                border: none;
                border-radius: 3px;
                font-size: 14px;
            }
            QPushButton:hover {
                background-color: #0069d9;
            }
        ''')
        
        # 创建控件
        self.username_label = QLabel('用户名:')
        self.username_input = QLineEdit()
        
        self.password_label = QLabel('密码:')
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.EchoMode.Password)
        
        self.login_button = QPushButton('登录')
        self.login_button.clicked.connect(self.on_login)
        
        self.register_button = QPushButton('注册')
        self.register_button.clicked.connect(self.show_register_window)
        
        # 布局
        layout = QVBoxLayout()
        layout.addWidget(self.username_label)
        layout.addWidget(self.username_input)
        layout.addWidget(self.password_label)
        layout.addWidget(self.password_input)
        layout.addWidget(self.login_button)
        layout.addWidget(self.register_button)
        
        self.setLayout(layout)
    
    def on_login(self):
        username = self.username_input.text()
        password = self.password_input.text()
        
        if self.db.verify_user(username, password):
            QMessageBox.information(self, '登录成功', '欢迎回来！')
        else:
            QMessageBox.warning(self, '登录失败', '用户名或密码错误')
            
    def show_register_window(self):
        self.register_window = RegisterWindow(self.db, self)
        self.register_window.show()
        self.hide()
