import sqlite3

class Database:
    def __init__(self):
        self.db_path = 'users.db'
        self.conn = None
        self.connect()
        self.create_table()
        self.add_test_user()

    def connect(self, retries=5, delay=2):
        import time
        import os
        import psutil
        
        def is_db_locked():
            try:
                # 检查是否有其他进程占用数据库文件
                for proc in psutil.process_iter(['pid', 'name', 'open_files']):
                    try:
                        if proc.info['open_files']:
                            for f in proc.info['open_files']:
                                if f.path == os.path.abspath(self.db_path):
                                    return True
                    except (psutil.NoSuchProcess, psutil.AccessDenied):
                        continue
                return False
            except Exception:
                return False
                
        def force_unlock():
            try:
                # 删除可能存在的锁文件
                lock_files = [
                    self.db_path + '-wal',
                    self.db_path + '-shm',
                    self.db_path + '-journal'
                ]
                for lock_file in lock_files:
                    if os.path.exists(lock_file):
                        os.remove(lock_file)
            except Exception as e:
                print(f"Warning: Failed to remove lock files: {e}")

        for attempt in range(retries):
            try:
                # 检查并清理锁文件
                if attempt > 1:
                    force_unlock()
                    
                # 检查是否有其他进程占用
                if is_db_locked():
                    print(f"Database is locked by another process, retrying...")
                    time.sleep(delay)
                    continue
                    
                self.conn = sqlite3.connect(
                    self.db_path,
                    timeout=60,
                    isolation_level=None,
                    check_same_thread=False,
                    detect_types=sqlite3.PARSE_DECLTYPES
                )
                self.conn.execute('PRAGMA journal_mode=WAL;')
                self.conn.execute('PRAGMA busy_timeout=60000;')
                self.conn.execute('PRAGMA synchronous=NORMAL;')
                return
            except sqlite3.Error as e:
                print(f"Connection attempt {attempt + 1} failed: {str(e)}")
                if attempt < retries - 1:
                    time.sleep(delay)
                else:
                    raise RuntimeError(f"Failed to connect after {retries} attempts: {str(e)}")
        
    def create_table(self):
        query = '''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL
        )
        '''
        self.conn.execute(query)
        self.conn.commit()
        
    def add_test_user(self):
        # 添加测试用户
        try:
            self.conn.execute(
                "INSERT INTO users (username, password) VALUES (?, ?)",
                ('admin', '123456')
            )
            self.conn.commit()
        except sqlite3.IntegrityError:
            # 如果用户已存在则跳过
            pass
            
    def verify_user(self, username, password):
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT * FROM users WHERE username = ? AND password = ?",
            (username, password)
        )
        return cursor.fetchone() is not None
        
    def register_user(self, username, password):
        try:
            with self.conn:
                self.conn.execute(
                    "INSERT INTO users (username, password) VALUES (?, ?)",
                    (username, password)
                )
                return True
        except sqlite3.IntegrityError:
            return False
        except sqlite3.Error as e:
            print(f"Register user error: {e}")
            return False
        
    def __del__(self):
        self.conn.close()
